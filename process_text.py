import string
from utils import save_doc, load_doc, clean_doc

in_file_name = r'./data/the_republic.txt'
doc = load_doc(in_file_name)
print(doc[:200])

tokens = clean_doc(doc)
print(tokens[:200])
print('Total tokens: %d' % len(tokens))
print('Unique tokens: %d' % len(set(tokens)))

# organize tokens into sequences of 50 characters
length = 50 + 1 # inclusive
sequences = list()
for i in range(length, len(tokens)):
    seq = tokens[i-length:i] # maving window by 1 character for a sequence of 50
    line = ' '.join(seq) # create sentence from sequence of 50 characters
    sequences.append(line) # save sequence
print('Total sequences: %d' %len(sequences))

out_filename = r'.\data\republic_sequences.txt'
save_doc(sequences, out_filename)
