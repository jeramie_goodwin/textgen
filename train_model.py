from model import build_model
from numpy import array
from pickle import dump
from keras.utils import to_categorical
from keras.preprocessing.text import Tokenizer

from utils import load_doc

# load data
in_filename = r'.\data\republic_sequences.txt'
doc = load_doc(in_filename)
lines = doc.split('\n')

# encode text
tokenizer = Tokenizer()
tokenizer.fit_on_texts(lines)
sequences = tokenizer.texts_to_sequences(lines)

# vocab size
vocab_size = len(tokenizer.word_index)+1

sequences = array(sequences)

X, y = sequences[:, :-1], sequences[:, -1]
y = to_categorical(y, num_classes=vocab_size)
seq_length = X.shape[1]

model = build_model(vocab_size=vocab_size, num_layers=100, num_features=50, seq_length=seq_length, print_summary=True)

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
model.fit(X, y, batch_size=128, epochs=1)

model.save(r'.\models\republic_model.h5')
dump(tokenizer, open(r'.\models\republic_tokenizer.pkl', 'wb'))
