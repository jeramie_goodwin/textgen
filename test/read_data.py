import urllib.request
from urllib.error import HTTPError,URLError

URL = r'http://www.gutenberg.org/cache/epub/1497/pg1497.txt'
try:
    f = urllib.request.urlopen(URL)
    print("downloading")
    local_file = open("./data/the_republic.txt", "wb")

    local_file.write(f.read())
    local_file.close()
except HTTPError as e:
    print("HTTP Error:", e.code, URL)
except URLError as e:
    print("URL Error:", e.code, URL)
