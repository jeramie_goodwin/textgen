from model import build_model
from numpy import array
from pickle import dump
from keras.utils import to_categorical
from keras.preprocessing.text import Tokenizer
import keras.callbacks as callbacks

from utils import load_doc
from utils import generate_sequences
from random import randint
from pickle import load
from keras.models import load_model

# load data
in_filename = './data/republic_sequences.txt'
doc = load_doc(in_filename)
lines = doc.split('\n')

# encode text
tokenizer = Tokenizer()
tokenizer.fit_on_texts(lines)
sequences = tokenizer.texts_to_sequences(lines)

# vocab size
vocab_size = len(tokenizer.word_index)+1

sequences = array(sequences)

X, y = sequences[:, :-1], sequences[:, -1]
y = to_categorical(y, num_classes=vocab_size)
seq_length = X.shape[1]

model = build_model(vocab_size=vocab_size, num_layers=100, num_features=50, seq_length=seq_length, print_summary=True)
#cb = callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=0, verbose=0, mode='auto', baseline=None, restore_best_weights=False)

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])#, callbacks=[cb])
model.fit(X, y, batch_size=128, epochs=1)

model.save(r'.\models\republic_model.h5')
dump(tokenizer, open(r'.\models\republic_tokenizer.pkl', 'wb'))

seq_length = len(lines[0].split()) - 1

model = load_model(r'.\models\republic_model.h5')
tokenizer = load(open(r'.\models\republic_tokenizer.pkl', 'rb'))

seed_text = lines[randint(0, len(lines))]
print(seed_text + '\n')

generated = generate_sequences(model, tokenizer, seq_length, seed_text, 50)

print(generated)
