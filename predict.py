from utils import generate_sequences
from utils import load_doc
from random import randint
from pickle import load
from keras.models import load_model

in_filename = r'.\data\republic_sequences.txt'
doc = load_doc(in_filename)
lines = doc.split('\n')
seq_length = len(lines[0].split()) - 1

model = load_model(r'.\TextGen\models\republic_model.h5')
tokenizer = load(open(r'.\TextGen\models\republic_tokenizer.pkl', 'rb'))

seed_text = lines[randint(0, len(lines))]
print(seed_text + '\n')

generated = generate_sequences(model, tokenizer, seq_length, seed_text, 50)
