from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Embedding

def build_model(vocab_size, seq_length, num_features=50, num_layers=100, print_summary=False):
    model = Sequential()
    model.add(Embedding(vocab_size, num_features, input_length=seq_length))
    model.add(LSTM(num_layers, return_sequences=True))
    model.add(LSTM(num_layers))
    model.add(Dense(num_layers, activation='relu'))
    model.add(Dense(vocab_size, activation='softmax'))

    if print_summary:
        print(model.summary())

    return model
